﻿using Meep.Tech.ModularData;
using UnityEngine;

namespace Meep.Tech.Games.Unity.Data {

  /// <summary>
  /// Get and set an image field for a model using a unity Texture2D
  /// </summary>
  public class Texture2DModelImageFieldAttribute : ModelImageFieldAttribute {

    /// <summary>
    /// Texture 2D to PNG bytes
    /// </summary>
    protected override byte[] EncodeToBytes(object image) {
      return (image as Texture2D)?.EncodeToPNG();
    }

    /// <summary>
    /// PNG bytes to texture 2D
    /// </summary>
    protected override object DeserializeFromBytes(byte[] imageBytes) {
      //creates texture and loads byte array data to create image
      Texture2D texture = new Texture2D(2, 2);
      texture.LoadImage(imageBytes);

      // Creates a new Sprite based on the Texture2D
      return texture;
    }

    /// <summary>
    /// passthrough ctor
    /// </summary>
    public Texture2DModelImageFieldAttribute(string DirtyFlagBooleanFieldName = null) 
      : base(DirtyFlagBooleanFieldName) {}
  }
}
